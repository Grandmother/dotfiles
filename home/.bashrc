#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source ~/.bashrc_common

alias vim='vim --servername VIM'

source "$HOME/.homesick/repos/homeshick/homeshick.sh"
source "$HOME/.homesick/repos/homeshick/completions/homeshick-completion.bash"

export PATH=${PATH}:${HOME}/bin/
export PATH=${PATH}:${HOME}/.local/bin

export HISTSIZE=5000
