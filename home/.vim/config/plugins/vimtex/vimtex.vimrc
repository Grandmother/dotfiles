let g:vimtex_compiler_latexmk = { 
        \ 'executable' : 'latexmk',
        \ 'options' : [ 
        \   '-pdf',
        \   '-pdflatex="xelatex -synctex=1 %S %O"',
        \   '-verbose',
        \   '-file-line-error',
        \   '-interaction=nonstopmode'
        \ ],
        \}

let g:vimtex_view_general_viewer="mupdf"
let g:vimtex_view_general_options="-r 80 @pdf"
