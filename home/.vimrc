if ! &diff
call plug#begin('~/.vim/plugged')

Plug 'majutsushi/tagbar'                      " show code tags in sidebar
Plug 'craigemery/vim-autotag'                 " Using ctags for C-code
Plug 'vim-scripts/ifdef-highlighting'         " Highlight ifdef branches
Plug 'vim-scripts/DoxygenToolkit.vim'         " Doxygen vim plugin

Plug 'scrooloose/nerdtree'                    " NERD-tree -- plugin for showing the tree of project files
Plug 'AndrewRadev/linediff.vim'               " get diff between lines within single file
Plug 'dhruvasagar/vim-table-mode'             " Draw tables in vim

Plug 'scrooloose/nerdcommenter'               " NERD-commenter -- plugin for commenting code fairly easy
Plug 'Townk/vim-autoclose'                    " Close braces automatically
Plug 'Chiel92/vim-autoformat'

Plug 'airblade/vim-gitgutter'                 " Show git diff in sidebar. Mark changes to stage

Plug 'xolox/vim-misc'                         " needed by vim-lua-ftplugin
Plug 'xolox/vim-lua-ftplugin'                 " Lua filetype plugin
Plug 'w0rp/ale'                               " Code linters
Plug 'lervag/vimtex'                          " Work with TeX files
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'wannesm/wmgraphviz.vim'
Plug 'junegunn/vim-easy-align'

Plug 'beautify-web/js-beautify'               " JavaScript beautifier

Plug 'thinca/vim-quickrun'                    " Plugin that runs code (c++/python/etc.)
call plug#end()
endif " ! &diff

set encoding=utf-8

source ~/.vim/colours.vim

source ~/.vim/config/plugins/vimtex/vimtex.vimrc
source ~/.vim/config/global/global.vimrc
source ~/.vim/config/lang/python/python.vimrc
source ~/.vim/config/lang/cpp/ale.vimrc

"
" VIEW
"
set number          " Show line numbers
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set list

" Tabs  I decided to use the first way, described in tabstop documentation
set softtabstop=4
set shiftwidth=4

set expandtab

set autoindent
set smartindent

set textwidth=80
set colorcolumn=81

"Wildmenu
set wildmenu
set wcm=<Tab>

set mouse=a

syntax on           " Switch syntax highlighting on

" Search
set ignorecase
set smartcase
set hlsearch
set incsearch
"
" Plugins configuration
"

"Status string
fun! <SID>SetStatusLine()
    let l:s1="%-3.3n\\ %f\\ %h%m%r%w"
    let l:s2="[%{strlen(&filetype)?&filetype:'?'},%{&encoding},%{&fileformat}]"
    let l:s3="%=\\ 0x%-8B\\ \\ %-14.(%l,%c%V%)\\ %<%P"
    execute "set statusline=" . l:s1 . l:s2 . l:s3
endfun
set laststatus=2
call <SID>SetStatusLine()


" Key mappings

" hjkl -> jkl;
noremap j h
noremap k j
noremap l k
noremap ; l

" Backups and swapfiles
set directory=$HOME/.vim/swapfiles//
set backup
set backupdir=$HOME/.vim/backups//

"
" Key mappings
"

" <F2> save
nmap <F2> :w!<CR>
imap <F2> <Esc>:w!<CR>i
vmap <F2> <Esc>:w!<CR>v

" <F3> NERDTree
nmap <F3> :NERDTreeToggle<CR>

" <F4> go to source/header
map <F4> :e %:p:s,.h$,.X123X,:s,.c$,.h,:s,.X123X$,.c,<CR>

" <F7> Tagbar toggle
" MAJUTSUSHI/TAGBAR
nmap <silent> <F7> :TagbarToggle<CR>
let g:tagbar_autoclose=1
" let g:tagbar_ctags_bin="/usr/bin/ctags"
" got configuration from https://github.com/jstemmer/gotags
setlocal updatetime=400

" <F8> switch Encoding
menu Encoding.koi8-r :e ++enc=koi8-r ++ff=unix<CR>
menu Encoding.windows-1251 :e ++enc=cp1251 ++ff=dos<CR>
menu Encoding.cp866 :e ++enc=cp866 ++ff=dos<CR>
menu Encoding.utf-8 :e ++enc=utf8 <CR>
menu Encoding.koi8-u :e ++enc=koi8-u ++ff=unix<CR>
map <F8> :emenu Encoding.<TAB>

" <F9> close preview windows
nmap <F9> <C-w>z
imap <F9> <C-w>z
vmap <F9> <C-w>z

" <F10> qiut
nmap <F10> :q<CR>
imap <F10> <Esc><Esc>:q<CR>
vmap <F10> <Esc>:q<CR>

" Move window
nnoremap <C-j> <C-w>h
nnoremap <C-k> <C-w>j
nnoremap <C-l> <C-w>k
nnoremap <C-semicolon> <C-w>l

" Plugins configuration

" Use deoplete
let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_start_length = 1
autocmd CompleteDone * pclose

"Status tring
fun! <SID>SetStatusLine()
    let l:s1="%-3.3n\\ %f\\ %h%m%r%w"
    let l:s2="[%{strlen(&filetype)?&filetype:'?'},%{&encoding},%{&fileformat}]"
    let l:s3="%=\\ 0x%-8B\\ \\ %-14.(%l,%c%V%)\\ %<%P"
    execute "set statusline=" . l:s1 . l:s2 . l:s3
endfun
set laststatus=2
call <SID>SetStatusLine()

set wildmenu
set wcm=<Tab>

" project specific configs and modeline
set secure          " make preject specific .vimrc secure
set exrc            " Use project specific .vimrc
set modeline
